From c2dea135f5f1c097064e6cf887fb51273b10e33c Mon Sep 17 00:00:00 2001
From: Andrew Morgan <andrew@amorgan.xyz>
Date: Wed, 4 Dec 2024 18:35:17 +0000
Subject: [PATCH] Explicitly list func arguments

mypy would not accept passing a **kwargs to `HostnameEndpoint`.

Resolves the following errors:

 synapse/http/proxyagent.py:385: error: Argument 4 to "HostnameEndpoint" has incompatible type "**dict[str, object]"; expected "float"  [arg-type]
synapse/http/proxyagent.py:385: error: Argument 4 to "HostnameEndpoint" has incompatible type "**dict[str, object]"; expected "Union[bytes, str, tuple[Union[bytes, str], int], None]"  [arg-type]
synapse/http/proxyagent.py:385: error: Argument 4 to "HostnameEndpoint" has incompatible type "**dict[str, object]"; expected "Optional[float]"  [arg-type]
---
 synapse/http/proxyagent.py | 10 +++++++---
 1 file changed, 7 insertions(+), 3 deletions(-)

diff --git a/synapse/http/proxyagent.py b/synapse/http/proxyagent.py
index f80f67acc6..36cb9285df 100644
--- a/synapse/http/proxyagent.py
+++ b/synapse/http/proxyagent.py
@@ -21,7 +21,7 @@
 import logging
 import random
 import re
-from typing import Any, Collection, Dict, List, Optional, Sequence, Tuple
+from typing import Any, Collection, Dict, List, Optional, Sequence, Tuple, Union
 from urllib.parse import urlparse
 from urllib.request import (  # type: ignore[attr-defined]
     getproxies_environment,
@@ -351,7 +351,9 @@ def http_proxy_endpoint(
     proxy: Optional[bytes],
     reactor: IReactorCore,
     tls_options_factory: Optional[IPolicyForHTTPS],
-    **kwargs: object,
+    timeout: float = 30,
+    bindAddress: Optional[Union[bytes, str, tuple[Union[bytes, str], int]]] = None,
+    attemptDelay: Optional[float] = None,
 ) -> Tuple[Optional[IStreamClientEndpoint], Optional[ProxyCredentials]]:
     """Parses an http proxy setting and returns an endpoint for the proxy
 
@@ -382,7 +384,9 @@ def http_proxy_endpoint(
     # 3.9+) on scheme-less proxies, e.g. host:port.
     scheme, host, port, credentials = parse_proxy(proxy)
 
-    proxy_endpoint = HostnameEndpoint(reactor, host, port, **kwargs)
+    proxy_endpoint = HostnameEndpoint(
+        reactor, host, port, timeout, bindAddress, attemptDelay
+    )
 
     if scheme == b"https":
         if tls_options_factory:
-- 
2.46.2

