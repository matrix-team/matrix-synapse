#!/bin/sh

dch -v"$1" New upstream release.
git add debian/changelog
debcommit -m "Prepare changelog entry"
